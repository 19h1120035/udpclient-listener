﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Udp Client";
            Console.Write("Server IP: ");
            IPAddress ip = IPAddress.Parse(Console.ReadLine());

            UdpClient client = new UdpClient();
            // Lệnh connect này chỉ giúp lưu lại thông tin về đối tác
            // trong object client chứ không phải thực hiện kết nối như trong tcp socket
            client.Connect(ip, 1308);

            while (true)
            {
                Console.Write("$ Text >>> ");
                var text = Console.ReadLine();
                if (text == "exit") break;
                byte[] buffer = Encoding.ASCII.GetBytes(text);
                client.Send(buffer, buffer.Length);
                IPEndPoint dumpEp = new IPEndPoint(0, 0);
                buffer = client.Receive(ref dumpEp);
                string response = Encoding.ASCII.GetString(buffer);
                Console.WriteLine("$ Serer >>> "+response);
            }
        }
    }
}
