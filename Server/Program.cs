﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Udp Server";
            // Tự đông bind với cổng
            UdpClient server = new UdpClient(1308);
            while (true)
            {
                IPEndPoint remoteEp = new IPEndPoint(0, 0);
                byte[] buffer = server.Receive(ref remoteEp);
                if (buffer.Length == 0) break;
                string text = Encoding.ASCII.GetString(buffer);
                Console.WriteLine("# Client >>> " + text);
                string response = text.ToUpper();
                buffer = Encoding.ASCII.GetBytes(response);
                server.Send(buffer, buffer.Length, remoteEp);
            }

        }
    }
}
